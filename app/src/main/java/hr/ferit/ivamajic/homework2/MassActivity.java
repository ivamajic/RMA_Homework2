package hr.ferit.ivamajic.homework2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MassActivity extends AppCompatActivity {

    //Butterknife views
    @BindView(R.id.spinnerMass1) Spinner spinnerMass1;
    @BindView(R.id.spinnerMass2) Spinner spinnerMass2;
    @BindView(R.id.massValue)    EditText massValue;
    @BindView(R.id.bConvertMass) Button bCalculate;
    @BindView(R.id.bInvertMass) ImageView bInvert;

    //unit1->kg
    static final double DAG_TO_KG = 0.01;
    static final double G_TO_KG = 0.001;
    static final double MG_TO_KG = 0.000001;
    static final double UG_TO_KG = 0.000000001;
    static final double T_TO_KG = 1000;
    static final double LB_TO_KG = 0.45359237;

    //kg->unit2
    static final double KG_TO_DAG = 100;
    static final double KG_TO_G = 1000;
    static final double KG_TO_MG = 1_000_000;
    static final double KG_TO_UG = 1_000_000_000;
    static final double KG_TO_T = 0.001;
    static final double KG_TO_LB = 2.20462262;

    //units
    String units[]=new String[]{"kilogram","decagram","gram","milligram","microgram","metric ton","pound" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mass);

        ButterKnife.bind(this);

        //spinners
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, R.layout.spinner_layout, units);
        adapter1.setDropDownViewResource(R.layout.spinner_layout);
        spinnerMass1.setAdapter(adapter1);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, R.layout.spinner_layout, units);
        adapter2.setDropDownViewResource(R.layout.spinner_layout);
        spinnerMass2.setAdapter(adapter2);
    }

    @OnClick(R.id.bInvertMass)
    public void invert() {
        //unit1-><-unit2
        int unit1 = spinnerMass1.getSelectedItemPosition();
        int unit2 = spinnerMass2.getSelectedItemPosition();

        spinnerMass1.setSelection(unit2);
        spinnerMass2.setSelection(unit1);

    }

    @OnClick(R.id.bConvertMass)
    public void convert() {
        String value = massValue.getText().toString();

        if (!value.equals("")) {
            String unit1 = spinnerMass1.getSelectedItem().toString();
            String unit2 = spinnerMass2.getSelectedItem().toString();
            double valueN = Double.parseDouble(massValue.getText().toString());
            double temp, resultN;

            //unit1->kg
            switch (unit1) {
                case "kilogram":
                    temp = valueN;
                    break;
                case "decagram":
                    temp = valueN * DAG_TO_KG;
                    break;
                case "gram":
                    temp = valueN * G_TO_KG;
                    break;
                case "milligram":
                    temp = valueN * MG_TO_KG;
                    break;
                case "microgram":
                    temp = valueN * UG_TO_KG;
                    break;
                case "metric ton":
                    temp = valueN * T_TO_KG;
                    break;
                case "pound":
                    temp = valueN * LB_TO_KG;
                    break;
                default:
                    temp = 0;
                    break;
            }

            //kg->unit2
            switch (unit2) {
                case "kilogram":
                    resultN = temp;
                    break;
                case "decagram":
                    resultN = temp * KG_TO_DAG;
                    break;
                case "gram":
                    resultN = temp * KG_TO_G;
                    break;
                case "milligram":
                    resultN = temp * KG_TO_MG;
                    break;
                case "microgram":
                    resultN = temp * KG_TO_UG;
                    break;
                case "metric ton":
                    resultN = temp * KG_TO_T;
                    break;
                case "pound":
                    resultN = temp * KG_TO_LB;
                    break;
                default:
                    resultN = 0;
                    break;
            }

            //Intent->ResultActivity
            Intent resultShow = new Intent(this, ResultActivity.class);
            String result = String.valueOf(resultN);
            resultShow.putExtra(ResultActivity.UNIT1, unit1);
            resultShow.putExtra(ResultActivity.UNIT2, unit2);
            resultShow.putExtra(ResultActivity.RESULT_VALUE1, value);
            resultShow.putExtra(ResultActivity.RESULT_VALUE2, result);
            startActivity(resultShow);
        }
    }
}
