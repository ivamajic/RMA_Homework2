package hr.ferit.ivamajic.homework2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TemperatureActivity extends AppCompatActivity {

    //Views
    @BindView(R.id.spinnerTemperature1) Spinner spinnerTemp1;
    @BindView(R.id.spinnerTemperature2) Spinner spinnerTemp2;
    @BindView(R.id.temperatureValue) EditText tempValue;
    @BindView(R.id.bConvertTemperature) Button bCalculate;
    @BindView(R.id.bInvertTemperature)  ImageView bInvert;

    String units[]=new String[]{"Fahrenheit","Celsius","Kelvin","Rankine","Delisle","Newton","Réamur","Rømer"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);

        ButterKnife.bind(this);

        //Spinners
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, R.layout.spinner_layout, units);
        adapter1.setDropDownViewResource(R.layout.spinner_layout);
        spinnerTemp1.setAdapter(adapter1);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, R.layout.spinner_layout, units);
        adapter2.setDropDownViewResource(R.layout.spinner_layout);
        spinnerTemp2.setAdapter(adapter2);
    }

    @OnClick(R.id.bInvertTemperature)
    public void invert() {
        //unit1-><-unit2
        int unit1 = spinnerTemp1.getSelectedItemPosition();
        int unit2 = spinnerTemp2.getSelectedItemPosition();

        spinnerTemp1.setSelection(unit2);
        spinnerTemp2.setSelection(unit1);

    }

    @OnClick(R.id.bConvertTemperature)
    public void convert() {

        String value = tempValue.getText().toString();

        if (!value.equals("")) {
            String unit1 = spinnerTemp1.getSelectedItem().toString();
            String unit2 = spinnerTemp2.getSelectedItem().toString();
            double valueN = Double.parseDouble(tempValue.getText().toString());
            double temp, resultN;

            //unit1->Celsius
            switch (unit1) {
                case "Celsius":
                    temp = valueN;
                    break;
                case "Fahrenheit":
                    temp = (valueN - 32) / 1.8;
                    break;
                case "Kelvin":
                    temp = valueN - 273.15;
                    break;
                case "Rankine":
                    temp = (valueN - 491.67) * 5 / 9;
                    break;
                case "Delisle":
                    temp = (valueN + 100) / 1.5;
                    break;
                case "Newton":
                    temp = valueN * 100 / 33;
                    break;
                case "Réamur":
                    temp = valueN / 0.8;
                    break;
                case "Rømer":
                    temp = (valueN - 7.5) / 0.525;
                    break;
                default:
                    temp = 0;
                    break;
            }

            //Celsius->unit2
            switch (unit2) {
                case "Celsius":
                    resultN = temp;
                    break;
                case "Fahrenheit":
                    resultN = temp * 1.8 + 32;
                    break;
                case "Kelvin":
                    resultN = temp + 273.15;
                    break;
                case "Rankine":
                    resultN = (temp + 273.15) * 9 / 5;
                    break;
                case "Delisle":
                    resultN = (100 - temp) * 1.5;
                    break;
                case "Newton":
                    resultN = temp * 33 / 100;
                    break;
                case "Réamur":
                    resultN = temp * 4 / 5;
                    break;
                case "Rømer":
                    resultN = temp * 21 / 40 + 7.5;
                    break;
                default:
                    resultN = 0;
                    break;
            }

            //intent->ResultActivity
            Intent resultShow = new Intent(this, ResultActivity.class);
            String result = String.valueOf(resultN);
            resultShow.putExtra(ResultActivity.UNIT1, unit1);
            resultShow.putExtra(ResultActivity.UNIT2, unit2);
            resultShow.putExtra(ResultActivity.RESULT_VALUE1, value);
            resultShow.putExtra(ResultActivity.RESULT_VALUE2, result);
            startActivity(resultShow);
        }
    }
}
