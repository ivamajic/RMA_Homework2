package hr.ferit.ivamajic.homework2;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.ivTemperature) ImageView ivTemperature;
    @BindView(R.id.ivMass) ImageView ivMass;
    @BindView(R.id.ivCurrency) ImageView ivCurrency;
    @BindView(R.id.ivLength) ImageView ivLength;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }


    @OnClick({R.id.ivLength, R.id.ivCurrency, R.id.ivMass, R.id.ivTemperature})
    public void openConverter(ImageView imageview) {
        Intent action;
        switch (imageview.getId()) {
            case R.id.ivLength:
                action = new Intent(this, LengthActivity.class);
                break;
            case R.id.ivCurrency:
                action = new Intent(this, CurrencyActivity.class);
                break;
            case R.id.ivMass:
                action = new Intent(this, MassActivity.class);
                break;
            default:
                action = new Intent(this, TemperatureActivity.class);
                break;
        }
        startActivity(action);
    }
}
