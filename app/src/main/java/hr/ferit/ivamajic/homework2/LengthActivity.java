package hr.ferit.ivamajic.homework2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LengthActivity extends AppCompatActivity {

    //Views
    @BindView(R.id.spinnerLength1)    Spinner spinnerLength1;
    @BindView(R.id.spinnerLength2) Spinner spinnerLength2;
    @BindView(R.id.lengthValue)    EditText lengthValue;
    @BindView(R.id.bConvertLength)    Button bCalculate;
    @BindView(R.id.bInvertLength)    ImageView bInvert;

    //unit1 -> meter
    static final int KILOMETER_TO_METER = 1000;
    static final double DECIMETER_TO_METER = 0.1;
    static final double CENTIMETER_TO_METER = 0.01;
    static final double MILLIMETER_TO_METER = 0.001;
    static final double MICROMETER_TO_METER = 0.000001;
    static final double INCH_TO_METER = 0.0254;
    static final double YARD_TO_METER = 0.9144;
    static final double FOOT_TO_METER = 0.3048;
    static final double MILE_TO_METER = 1609.344;
    static final int LEAGUE_TO_METER = 5556;
    static final int NMILE_TO_METER = 1852;

    //meter -> unit2
    static final double METER_TO_KILOMETER = 0.001;
    static final double METER_TO_DECIMETER = 10;
    static final double METER_TO_CENTIMETER = 100;
    static final double METER_TO_MILLIMETER = 1000;
    static final double METER_TO_MICROMETER = 1000000;
    static final double METER_TO_INCH = 39.3700787;
    static final double METER_TO_YARD = 1.0936133;
    static final double METER_TO_FOOT = 3.2808399;
    static final double METER_TO_MILE = 0.000621371192;
    static final double METER_TO_LEAGUE = 0.000179985601;
    static final double METER_TO_NMILE = 0.000539956803;

    //units
    String units[]=new String[]{"kilometer","meter","decimeter","centimeter","millimeter","micrometer","inch","foot","yard","mile","league","nautical mile"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_length);


        ButterKnife.bind(this);

        //spinners
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, R.layout.spinner_layout, units);
        adapter1.setDropDownViewResource(R.layout.spinner_layout);
        spinnerLength1.setAdapter(adapter1);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, R.layout.spinner_layout, units);
        adapter2.setDropDownViewResource(R.layout.spinner_layout);
        spinnerLength2.setAdapter(adapter2);
    }

    @OnClick(R.id.bInvertLength)
    public void invert() {
        //unit1-><-unit2
        int unit1 = spinnerLength1.getSelectedItemPosition();
        int unit2 = spinnerLength2.getSelectedItemPosition();

        spinnerLength1.setSelection(unit2);
        spinnerLength2.setSelection(unit1);

    }

    @OnClick(R.id.bConvertLength)
    public void convert() {

        String value = lengthValue.getText().toString();

        if (!value.equals("")) {
            String unit1 = spinnerLength1.getSelectedItem().toString();
            String unit2 = spinnerLength2.getSelectedItem().toString();
            double valueN = Double.parseDouble(lengthValue.getText().toString());
            double temp, resultN;

            //unit1 -> meters
            switch (unit1) {
                case "kilometer":
                    temp = valueN * KILOMETER_TO_METER;
                    break;
                case "meter":
                    temp = valueN;
                    break;
                case "decimeter":
                    temp = valueN * DECIMETER_TO_METER;
                    break;
                case "centimeter":
                    temp = valueN * CENTIMETER_TO_METER;
                    break;
                case "millimeter":
                    temp = valueN * MILLIMETER_TO_METER;
                    break;
                case "micrometer":
                    temp = valueN * MICROMETER_TO_METER;
                    break;
                case "inch":
                    temp = valueN * INCH_TO_METER;
                    break;
                case "foot":
                    temp = valueN * FOOT_TO_METER;
                    break;
                case "yard":
                    temp = valueN * YARD_TO_METER;
                    break;
                case "mile":
                    temp = valueN * MILE_TO_METER;
                    break;
                case "league":
                    temp = valueN * LEAGUE_TO_METER;
                    break;
                case "nautical mile":
                    temp = valueN * NMILE_TO_METER;
                    break;
                default:
                    temp = 0;
                    break;
            }

            //meters -> unit2
            switch (unit2) {
                case "kilometer":
                    resultN = temp * METER_TO_KILOMETER;
                    break;
                case "meter":
                    resultN = temp;
                    break;
                case "decimeter":
                    resultN = temp * METER_TO_DECIMETER;
                    break;
                case "centimeter":
                    resultN = temp * METER_TO_CENTIMETER;
                    break;
                case "millimeter":
                    resultN = temp * METER_TO_MILLIMETER;
                    break;
                case "micrometer":
                    resultN = temp * METER_TO_MICROMETER;
                    break;
                case "inch":
                    resultN = temp * METER_TO_INCH;
                    break;
                case "foot":
                    resultN = temp * METER_TO_FOOT;
                    break;
                case "yard":
                    resultN = temp * METER_TO_YARD;
                    break;
                case "mile":
                    resultN = temp * METER_TO_MILE;
                    break;
                case "league":
                    resultN = temp * METER_TO_LEAGUE;
                    break;
                case "nautical mile":
                    resultN = temp * METER_TO_NMILE;
                    break;
                default:
                    resultN = 0;
                    break;
            }

            //intent -> ResultActivity
            Intent resultShow = new Intent(this, ResultActivity.class);
            String result = String.valueOf(resultN);
            resultShow.putExtra(ResultActivity.UNIT1, unit1);
            resultShow.putExtra(ResultActivity.UNIT2, unit2);
            resultShow.putExtra(ResultActivity.RESULT_VALUE1, value);
            resultShow.putExtra(ResultActivity.RESULT_VALUE2, result);
            startActivity(resultShow);
        }

    }
}
