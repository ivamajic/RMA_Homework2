package hr.ferit.ivamajic.homework2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CurrencyActivity extends AppCompatActivity {

    //Buterknife Views
    @BindView(R.id.spinnerCurrency1)    Spinner spinnerCurr1;
    @BindView(R.id.spinnerCurrency2) Spinner spinnerCurr2;
    @BindView(R.id.currencyValue)    EditText currValue;
    @BindView(R.id.bConvertCurrency)    Button bCalculate;
    @BindView(R.id.bInvertCurrency)    ImageView bInvert;

    //unit1->HRK
    static final double EUR_TO_HRK = 7.43876689;
    static final double USD_TO_HRK = 6.03281853;
    static final double GBP_TO_HRK = 8.45559846;
    static final double JPY_TO_HRK = 0.0567688224;
    static final double AUD_TO_HRK = 4.6301279;
    static final double CAD_TO_HRK = 4.67241795;
    static final double CHF_TO_HRK = 6.32287645;

    //HRK->unit2
    static final double HRK_TO_EUR = 0.134430883;
    static final double HRK_TO_USD = 0.16576;
    static final double HRK_TO_GBP = 0.11826484;
    static final double HRK_TO_JPY = 17.6153029;
    static final double HRK_TO_AUD = 0.215976755;
    static final double HRK_TO_CAD = 0.21402195;
    static final double HRK_TO_CHF = 0.158155866;

    //units
    String units[]=new String[]{"HRK","EUR","USD","GBP","JPY","AUD","CAD","CHF"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency);

        ButterKnife.bind(this);

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, R.layout.spinner_layout, units);
        adapter1.setDropDownViewResource(R.layout.spinner_layout);
        spinnerCurr1.setAdapter(adapter1);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, R.layout.spinner_layout, units);
        adapter2.setDropDownViewResource(R.layout.spinner_layout);
        spinnerCurr2.setAdapter(adapter2);
    }

    @OnClick(R.id.bInvertCurrency)
    public void invert() {
        //unit1-><-unit2
        int unit1 = spinnerCurr1.getSelectedItemPosition();
        int unit2 = spinnerCurr2.getSelectedItemPosition();

        spinnerCurr1.setSelection(unit2);
        spinnerCurr2.setSelection(unit1);

    }

    @OnClick(R.id.bConvertCurrency)
    public void convert() {

        String value = currValue.getText().toString();

        if (!value.equals("")) {
            String unit1 = spinnerCurr1.getSelectedItem().toString();
            String unit2 = spinnerCurr2.getSelectedItem().toString();
            double valueN = Double.parseDouble(value);
            double temp, resultN;

            //unit1->HRK
            switch(unit1) {
                case "HRK":
                    temp = valueN;
                    break;
                case "EUR":
                    temp = valueN * EUR_TO_HRK;
                    break;
                case "USD":
                    temp = valueN * USD_TO_HRK;
                    break;
                case "GBP":
                    temp = valueN * GBP_TO_HRK;
                    break;
                case "JPY":
                    temp = valueN * JPY_TO_HRK;
                    break;
                case "AUD":
                    temp = valueN * AUD_TO_HRK;
                    break;
                case "CAD":
                    temp = valueN * CAD_TO_HRK;
                    break;
                case "CHF":
                    temp = valueN * CHF_TO_HRK;
                    break;
                default:
                    temp = 0;
                    break;
            }

            //HRK->unit2
            switch(unit2) {
                case "HRK":
                    resultN = temp;
                    break;
                case "EUR":
                    resultN = temp * HRK_TO_EUR;
                    break;
                case "USD":
                    resultN = temp * HRK_TO_USD;
                    break;
                case "GBP":
                    resultN = temp * HRK_TO_GBP;
                    break;
                case "JPY":
                    resultN = temp * HRK_TO_JPY;
                    break;
                case "AUD":
                    resultN = temp * HRK_TO_AUD;
                    break;
                case "CAD":
                    resultN = temp * HRK_TO_CAD;
                    break;
                case "CHF":
                    resultN = temp * HRK_TO_CHF;
                    break;
                default:
                    resultN = 0;
                    break;
            }

            //intent->ResultActivity



            Intent resultShow = new Intent(this, ResultActivity.class);
            String result = String.valueOf(resultN);
            resultShow.putExtra(ResultActivity.UNIT1, unit1);
            resultShow.putExtra(ResultActivity.UNIT2, unit2);
            resultShow.putExtra(ResultActivity.RESULT_VALUE1, value);
            resultShow.putExtra(ResultActivity.RESULT_VALUE2, result);
            startActivity(resultShow);
        }
    }
}
