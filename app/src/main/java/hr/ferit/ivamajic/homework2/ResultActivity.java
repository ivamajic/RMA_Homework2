package hr.ferit.ivamajic.homework2;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResultActivity extends AppCompatActivity {

    public static final String UNIT1 = "unit1";
    public static final String UNIT2 = "unit2";
    public static final String RESULT_VALUE1 = "value";
    public static final String RESULT_VALUE2 = "result";

    @BindView(R.id.tvResultUnit1) TextView tvResultUnit1;
    @BindView(R.id.tvResultUnit2) TextView tvResultUnit2;
    @BindView(R.id.tvResultValue1) TextView tvResultValue1;
    @BindView(R.id.tvResultValue2) TextView tvResultValue2;

    @SuppressLint("DefaultLocale")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

       ButterKnife.bind(this);

        String unit1 = getIntent().getStringExtra(UNIT1);
        String unit2 = getIntent().getStringExtra(UNIT2);
        String resultValue1 = getIntent().getStringExtra(RESULT_VALUE1);
        String resultValue2 = getIntent().getStringExtra(RESULT_VALUE2);

        tvResultUnit1.setText(unit1);
        tvResultUnit2.setText(unit2);
        tvResultValue1.setText(resultValue1);
        tvResultValue2.setText(resultValue2);
        }
}
