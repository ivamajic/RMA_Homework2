# Homework project 2 - Unit Converter - UniCon

### The assignment and problems encountered

* The assignment was to make a unit converter with four different conversion types.

* I added a main activity which consists of four ImageViews, one for each conversion type.
* I added another four activities, again - one for each conversion type. These activities are accessed via the ImageViews on the main activity.
* The last activity displays the result of the conversion and it's the same for all conversion types.

* The biggest problem I faced while working on this assignment were spinners. At first I didn't know how to use them, but the official documentation made it a lot easier.

### Utilised libraries

* I used [Butterknife] (http://jakewharton.github.io/butterknife/) to bind views and make my code simpler.
* In order to learn how to use spinners, I consulted [Android Developers](https://developer.android.com/guide/topics/ui/controls/spinner.html).

### Screenshots

![MainActivity](/Screenshots/Screenshot1.png)
![MassActivity](/Screenshots/Screenshot2.png)
![MassActivity](/Screenshots/Screenshot3.png)
![ResultActivity](/Screenshots/Screenshot4.png)
![CurrencyActivity](/Screenshots/Screenshot5.png)
![CurrencyActivity](/Screenshots/Screenshot6.png)
![ResultActivity](/Screenshots/Screenshot7.png)


### Completed Tasks

- [x] Make a new Android project with a total of six activities
- [x] Add four buttons (which describe the conversion type) to the main activity
- [x] Enable the start of each conversion activity via the four buttons (using explicit Intents)
- [x] In each activity, get the user input, units (using spinners) and calculate the result
- [x] Send the result to the result activity
- [x] Show the result on the result activity
- [x] Test the app *The app was tested on Samsung Galaxy S7 (API 24) and Nexus 4 virtual device (API 22).*
